# Trabalho 2: Integração de habilidades - 2022/2
**Disciplina: Redes de Computadores**

**Curso: Engenharia de Computação / Elétrica**

**Nome/RA:**


## Tarefa 1:  Sub-Redes
| Sub- Rede |             IPv6 - Sub-Rede            |  IPv4 - Sub-Rede  |  IPv4 - Máscara   | IPv4 - Broadcast  |    
|:---------:|:--------------------------------------:|:-----------------:|:-----------------:|:-----------------:|
| Matriz    | 2001:DB8:ACAD:**NN** *00* ::/64 | 200.200.**N**.*??*   | 255.255.255.*??* | 200.200.**N**.*??*  |
| Filial 1  | 2001:DB8:ACAD:**NN** *01* ::/64 | 200.200.**N**.*??*  | 255.255.255.*??* | 200.200.**N**.*??*  |
| Filial 2  | 2001:DB8:ACAD:**NN** *02* ::/64 | 200.200.**N**.*??*  | 255.255.255.*??* | 200.200.**N**.*??* |
| Filial 3  | 2001:DB8:ACAD:**NN** *03* ::/64 | 200.200.**N**.*??* | 255.255.255.*??* | 200.200.**N**.*??* |
| Filial 4  | 2001:DB8:ACAD:**NN** *04* ::/64 | 200.200.**N**.*??* | 255.255.255.*??* | 200.200.**N**.*??* |
| Filial 5  | 2001:DB8:ACAD:**NN** *05* ::/64 | 200.200.**N**.*??* | 255.255.255.*??* | 200.200.**N**.*??* |
| pb-vit    | 2001:DB8:ACAD:**NN**FF:: *00* :0/112 | 200.200.**N**.*??* | 255.255.255.*??* | 200.200.**N**.*??* |
| vit-fb    | 2001:DB8:ACAD:**NN**FF:: *01* :0/112 | 200.200.**N**.*??* | 255.255.255.*??* | 200.200.**N**.*??* |
| fb-ita    | 2001:DB8:ACAD:**NN**FF:: *02* :0/112 | 200.200.**N**.*??* | 255.255.255.*??* | 200.200.**N**.*??* |
| ita-pb    | 2001:DB8:ACAD:**NN**FF:: *03* :0/112 | 200.200.**N**.*??* | 255.255.255.*??* | 200.200.**N**.*??* |
| cv-ita    | 2001:DB8:ACAD:**NN**FF:: *04* :0/112  | 200.200.**N**.*??* | 255.255.255.*??* | 200.200.**N**.*??* |


## Tarefa 2: Endereçamento de Dispositivos
| Dispositivo           |Interface|      IPv4     |  IPv4 - Máscara |IPv4 - Gateway|      IPv6/Prefixo (GUA)     | IPv6 (LLA) |IPv6-Gateway|
|:-----------------------:|:---------:|---------------|-----------------|--------------|-----------------------------|------------|---------|
| PC1                   | NIC     | 200.200.**N**.3   | 255.255.255.192 | 200.200.**N**.1  | 2001:DB8:ACAD:**NN**00::3/64    |   EUI-64   | FE80::1 |
| PC2                   | NIC     | 200.200.**N**.4   | 255.255.255.192 | 200.200.**N**.1  | 2001:DB8:ACAD:**NN**00::4/64    |   EUI-64   | FE80::1 |
| PC3                   | NIC     | 200.200.**N**.67  | 255.255.255.224 | 200.200.**N**.65 | 2001:DB8:ACAD:**NN**01::3/64    |   EUI-64   | FE80::1 |
| PC4                   | NIC     | 200.200.**N**.68  | 255.255.255.224 | 200.200.**N**.65 | 2001:DB8:ACAD:**NN**01::4/64    |   EUI-64   | FE80::1 |
| PC5                   | NIC     | 200.200.**N**.99  | 255.255.255.224 | 200.200.**N**.97 | 2001:DB8:ACAD:**NN**02::3/64    |   EUI-64   | FE80::1 |
| PC6                   | NIC     | 200.200.**N**.100 | 255.255.255.224 | 200.200.**N**.97 | 2001:DB8:ACAD:**NN**02::4/64    |   EUI-64   | FE80::1 |
| Switch-Matriz         | SVI     | 200.200.**N**.2   | 255.255.255.192 | 200.200.**N**.1  |             -               |     -      |    -    |
| Switch-Filial1        | SVI     | 200.200.**N**.66  | 255.255.255.224 | 200.200.**N**.65 |             -               |     -      |    -    |
| Switch-Filial2        | SVI     | 200.200.**N**.98  | 255.255.255.224 | 200.200.**N**.97 |             -               |     -      |    -    |
| Roteador-Pato Branco  | Fa0/0   | 200.200.**N**.1   | 255.255.255.192 |      -       | 2001:DB8:ACAD:**NN**00::1/64    |   FE80::1  |    -    |
| Roteador-Pato Branco  | Se0/0/0 | 200.200.**N**.225 | 255.255.255.252 |      -       | 2001:DB8:ACAD:**NN**FF::0:1/112 |   EUI-64   |    -    |
| Roteador-Pato Branco  | Se0/0/1 | 200.200.**N**.238 | 255.255.255.252 |      -       | 2001:DB8:ACAD:**NN**FF::3:2/112 |   EUI-64   |    -    |
| Roteador-Fco. Beltrão | Fa0/0   | 200.200.**N**.65  | 255.255.255.224 |      -       | 2001:DB8:ACAD:**NN**01::1/64    |   FE80::1  |    -    |
| Roteador-Fco. Beltrão | Se0/0/0 | 200.200.**N**.233 | 255.255.255.252 |      -       | 2001:DB8:ACAD:**NN**FF::2:1/112 |   EUI-64   |    -    |
| Roteador-Fco. Beltrão | Se0/0/1 | 200.200.**N**.230 | 255.255.255.252 |      -       | 2001:DB8:ACAD:**NN**FF::1:2/112 |   EUI-64   |    -    | 
| Roteador-Vitorino     | Se0/0/0 | 200.200.**N**.229 | 255.255.255.252 |      -       | 2001:DB8:ACAD:**NN**FF::1:1/112 |   EUI-64   |    -    | 
| Roteador-Vitorino     | Se0/0/1 | 200.200.**N**.226 | 255.255.255.252 |      -       | 2001:DB8:ACAD:**NN**FF::0:2/112 |   EUI-64   |    -    | 
| Roteador-Itapejara    | Se0/0/0 | 200.200.**N**.237 | 255.255.255.252 |      -       | 2001:DB8:ACAD:**NN**FF::3:1/112 |   EUI-64   |    -    | 
| Roteador-Itapejara    | Se0/0/1 | 200.200.**N**.234 | 255.255.255.252 |      -       | 2001:DB8:ACAD:**NN**FF::2:2/112 |   EUI-64   |    -    | 
| Roteador-Itapejara    | Fa0/1   | 200.200.**N**.241 | 255.255.255.252 |      -       | 2001:DB8:ACAD:**NN**FF::4:1/112 |   EUI-64   |    -    | 
| Roteador-Coronel      | Fa0/0   | 200.200.**N**.97  | 255.255.255.224 |      -       | 2001:DB8:ACAD:**NN**02::1/64    |   FE80::1  |    -    |
| Roteador-Coronel      | Fa0/1   | 200.200.**N**.242 | 255.255.255.252 |      -       | 2001:DB8:ACAD:**NN**FF::4:2/112 |   EUI-64   |    -    | 

## Tarefa 3: Tabela de Roteamento
#### IPv4

### Roteador Pato Branco
| Rede de Destino | Máscara | Next Hop | Interface de Saída |
|-----------------|---------|----------|--------------------|
| 200.200.**N**.64  | 255.255.255.224 | 200.200.**N**.226 | Se0/0/0 | 
| 200.200.**N**.228 | 255.255.255.252 | 200.200.**N**.226 | Se0/0/0 | 
| 200.200.**N**.232 | 255.255.255.252 | 200.200.**N**.226 | Se0/0/0 | 
| 200.200.**N**.240 | 255.255.255.252 | 200.200.**N**.226 | Se0/0/0 | 
| 200.200.**N**.96  | 255.255.255.224 | 200.200.**N**.226 | Se0/0/0 |

#### Roteador Francisco Beltrão
| Rede de Destino | Máscara | Next Hop | Interface de Saída |
|-----------------|---------|----------|--------------------|
| 200.200.**N**.0   | 255.255.255.192 | 200.200.**N**.234 | Se0/0/0 | 
| 200.200.**N**.224 | 255.255.255.252 | 200.200.**N**.234 | Se0/0/0 | 
| 200.200.**N**.236 | 255.255.255.252 | 200.200.**N**.234 | Se0/0/0 | 
| 200.200.**N**.240 | 255.255.255.252 | 200.200.**N**.234 | Se0/0/0 | 
| 200.200.**N**.96  | 255.255.255.224 | 200.200.**N**.234 | Se0/0/0 |

#### Roteador Vitorino
| Rede de Destino | Máscara | Next Hop | Interface de Saída |
|-----------------|---------|----------|--------------------|
| 200.200.**N**.0   | 255.255.255.192 | 200.200.**N**.230 | Se0/0/0 | 
| 200.200.**N**.64  | 255.255.255.224 | 200.200.**N**.230 | Se0/0/0 | 
| 200.200.**N**.232 | 255.255.255.252 | 200.200.**N**.230 | Se0/0/0 | 
| 200.200.**N**.236 | 255.255.255.252 | 200.200.**N**.230 | Se0/0/0 | 
| 200.200.**N**.96  | 255.255.255.224 | 200.200.**N**.230 | Se0/0/0 | 
| 200.200.**N**.240 | 255.255.255.252 | 200.200.**N**.230 | Se0/0/0 |

#### Roteador Itapejara D'Oeste
| Rede de Destino | Máscara | Next Hop | Interface de Saída |
|-----------------|---------|----------|--------------------|
| 200.200.**N**.0   | 255.255.255.192 | 200.200.**N**.238 | Se0/0/0 | 
| 200.200.**N**.64  | 255.255.255.224 | 200.200.**N**.238 | Se0/0/0 | 
| 200.200.**N**.224 | 255.255.255.252 | 200.200.**N**.238 | Se0/0/0 | 
| 200.200.**N**.228 | 255.255.255.252 | 200.200.**N**.238 | Se0/0/0 | 
| 200.200.**N**.96  | 255.255.255.224 | 200.200.**N**.242 | Fa0/1 | 

#### Roteador Coronel Vivida
| Rede de Destino | Máscara | Next Hop | Interface de Saída |
|-----------------|---------|----------|--------------------|
| 200.200.**N**.0   | 255.255.255.192 | 200.200.**N**.241 |  Fa0/1  | 
| 200.200.**N**.64  | 255.255.255.224 | 200.200.**N**.241 |  Fa0/1  | 
| 200.200.**N**.224 | 255.255.255.252 | 200.200.**N**.241 |  Fa0/1  | 
| 200.200.**N**.228 | 255.255.255.252 | 200.200.**N**.241 |  Fa0/1  | 
| 200.200.**N**.232 | 255.255.255.252 | 200.200.**N**.241 |  Fa0/1  | 
| 200.200.**N**.236 | 255.255.255.252 | 200.200.**N**.241 |  Fa0/1  | 


### IPv6
#### Roteador Pato Branco
| Rede de Destino/Prefixo      | Next Hop      | Interface de Saída |
|------------------------------|---------------|--------------------|
| 2001:0DB8:ACAD:**NN**01::/64     | 2001:0DB8:ACAD:**NN**FF::0:2 | Se0/0/0 |
| 2001:0DB8:ACAD:**NN**02::/64     | 2001:0DB8:ACAD:**NN**FF::0:2 | Se0/0/0 |
| 2001:0DB8:ACAD:**NN**FF::1:0/112 | 2001:0DB8:ACAD:**NN**FF::0:2 | Se0/0/0 |
| 2001:0DB8:ACAD:**NN**FF::2:0/112 | 2001:0DB8:ACAD:**NN**FF::0:2 | Se0/0/0 |
| 2001:0DB8:ACAD:**NN**FF::4:0/112 | 2001:0DB8:ACAD:**NN**FF::0:2 | Se0/0/0 |
      
#### Roteador Francisco Beltrão
| Rede de Destino/Prefixo      | Next Hop      | Interface de Saída |
|------------------------------|---------------|--------------------|   
| 2001:0DB8:ACAD:**NN**00::/64     | 2001:0DB8:ACAD:**NN**FF::2:2 | Se0/0/0 |
| 2001:0DB8:ACAD:**NN**02::/64     | 2001:0DB8:ACAD:**NN**FF::2:2 | Se0/0/0 |
| 2001:0DB8:ACAD:**NN**FF::0:0/112 | 2001:0DB8:ACAD:**NN**FF::2:2 | Se0/0/0 |
| 2001:0DB8:ACAD:**NN**FF::3:0/112 | 2001:0DB8:ACAD:**NN**FF::2:2 | Se0/0/0 |
| 2001:0DB8:ACAD:**NN**FF::4:0/112 | 2001:0DB8:ACAD:**NN**FF::2:2 | Se0/0/0 |
      
#### Roteador Vitorino
| Rede de Destino/Prefixo      | Next Hop      | Interface de Saída |
|------------------------------|---------------|--------------------|
| 2001:0DB8:ACAD:**NN**00::/64     | 2001:0DB8:ACAD:**NN**FF::1:2 | Se0/0/0 |
| 2001:0DB8:ACAD:**NN**01::/64     | 2001:0DB8:ACAD:**NN**FF::1:2 | Se0/0/0 |
| 2001:0DB8:ACAD:**NN**02::/64     | 2001:0DB8:ACAD:**NN**FF::1:2 | Se0/0/0 |
| 2001:0DB8:ACAD:**NN**FF::2:0/112 | 2001:0DB8:ACAD:**NN**FF::1:2 | Se0/0/0 |
| 2001:0DB8:ACAD:**NN**FF::3:0/112 | 2001:0DB8:ACAD:**NN**FF::1:2 | Se0/0/0 |
| 2001:0DB8:ACAD:**NN**FF::4:0/112 | 2001:0DB8:ACAD:**NN**FF::1:2 | Se0/0/0 |
      
#### Roteador Itapejara D'Oeste
| Rede de Destino/Prefixo      | Next Hop      | Interface de Saída |
|------------------------------|---------------|--------------------|
| 2001:0DB8:ACAD:**NN**00::/64     | 2001:0DB8:ACAD:**NN**FF::3:2 | Se0/0/0 |
| 2001:0DB8:ACAD:**NN**01::/64     | 2001:0DB8:ACAD:**NN**FF::3:2 | Se0/0/0 |
| 2001:0DB8:ACAD:**NN**02::/64     | 2001:0DB8:ACAD:**NN**FF::4:2 | Fa0/1   |
| 2001:0DB8:ACAD:**NN**FF::0:0/112 | 2001:0DB8:ACAD:**NN**FF::3:2 | Se0/0/0 |
| 2001:0DB8:ACAD:**NN**FF::1:0/112 | 2001:0DB8:ACAD:**NN**FF::3:2 | Se0/0/0 |
      
#### Roteador Coronel Vivida
| Rede de Destino/Prefixo      | Next Hop      | Interface de Saída |
|------------------------------|---------------|--------------------|
| 2001:0DB8:ACAD:**NN**00::/64     | 2001:0DB8:ACAD:**NN**FF::4:1 | Fa0/1   |
| 2001:0DB8:ACAD:**NN**01::/64     | 2001:0DB8:ACAD:**NN**FF::4:1 | Fa0/1   |
| 2001:0DB8:ACAD:**NN**FF::0:0/112 | 2001:0DB8:ACAD:**NN**FF::4:1 | Fa0/1   |
| 2001:0DB8:ACAD:**NN**FF::1:0/112 | 2001:0DB8:ACAD:**NN**FF::4:1 | Fa0/1   |
| 2001:0DB8:ACAD:**NN**FF::2:0/112 | 2001:0DB8:ACAD:**NN**FF::4:1 | Fa0/1   |
| 2001:0DB8:ACAD:**NN**FF::3:0/112 | 2001:0DB8:ACAD:**NN**FF::4:1 | Fa0/1   |


## Topologia - Packet Tracer
- [ ] ![Trabalho2-Topologia-NomeAluno](trabalho2-20222-topologia-NomeAluno.pkt)

